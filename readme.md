Sources: Heimdal (https://heimdalsecurity.com/blog/ransomware-decryption-tools/), Alternat0r (https://github.com/alternat0r/Ransomware-Decryptor-List), and others.

# Ransomware Decryptor List

![Status Incomplete](https://img.shields.io/badge/Status-Incomplete-orange.svg)

The following table will be a large list of ransomware decryptor. This is consist from various type of ransomware. To find your interest ransomware, please use your browser find input.

| Ransomware Variant | Decryptor Tool | Comments |
| --------------- | ------------- | ------ |
| Gandcrab v1     | https://www.bleepingcomputer.com/news/security/free-decrypter-available-for-gandcrab-ransomware-victims/ | N/A |
| Apocalypse      | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](http://now.avg.com/dont-pay-the-ransom-avg-releases-six-free-decryption-tools-to-retrieve-your-files/) | - |
| Ronnoh          | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](http://support.kaspersky.com/viruses/disinfection/8547?_ga=1.119880136.197632515.1463995280#block2) | - |
| CryptXXX        | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](http://www.bleepingcomputer.com/virus-removal/cryptxxx-ransomware-help-information#decrypt) | - |
| CryptXXX        | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](https://noransom.kaspersky.com/) | - |
| CryptXXX        | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](https://esupport.trendmicro.com/solution/en-US/1114221.aspx) | - |
| Crypt888,Mircop | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](http://files-download.avg.com/util/avgrem/avg_decryptor_Crypt888.exe) | - |
| Dharma          | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](https://www.bleepingcomputer.com/news/security/kaspersky-releases-decryptor-for-the-dharma-ransomware/#cid4721) | .dharma extension only |
| Xoris, 777      | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](http://www.bleepingcomputer.com/news/security/emsisoft-releases-decryptors-for-the-xorist-and-777-ransomware/) | - |
| Coinvault       | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://noransom.kaspersky.com/static/CoinVaultDecryptor.zip) | - |
| HydraCrypt and UmbreCrypt | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](http://blog.emsisoft.com/2016/02/12/decrypter-for-hydracrypt-and-umbrecrypt-available/) | - |
| Operation Global III | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](http://www.bleepingcomputer.com/forums/t/559220/operation-global-iii-ransomware-not-only-encrypts-but-infects-your-data-as-well/) | - |
| BadBlock        | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/badblock) | - |
| BadBlock        | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://files-download.avg.com/util/avgrem/avg_decryptor_BadBlock32.exe) | - |
| BadBlock        | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://files-download.avg.com/util/avgrem/avg_decryptor_BadBlock64.exe) | - |
| Xorist          | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/xorist) | - |
| Xorist          | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://media.kaspersky.com/utilities/VirusUtilities/EN/xoristdecryptor.zip) | - |
| 777             | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/777) | - |
| AutoLocky       | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/autolocky) | - |
| AutoLocky       | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://esupport.trendmicro.com/solution/en-US/1114221.aspx) | - |
| Nemucod         | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/nemucod) | - |
| DMALocker2      | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/dmalocker2) | - |
| DMALocker       | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/dmalocker) | - |
| CrypBoss        | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/crypboss) | - |
| Gomasom         | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/gomasom) | - |
| LeChiffre       | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/lechiffre) | - |
| Legion          | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://files-download.avg.com/util/avgrem/avg_decryptor_Legion.exe) | - |
| KeyBTC          | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/keybtc) | - |
| Radamant        | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/radamant) | - |
| CryptInfinite   | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/cryptinfinite) | - |
| PClock          | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/pclock) | - |
| CryptoDefense   | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/cryptodefense) | - |
| Harasom         | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://decrypter.emsisoft.com/download/harasom) | - |
| Decrypt Protect | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://tmp.emsisoft.com/fw/decrypt_mblblock.exe) | - |
| Rakhni, helpme@freespeechmail.org | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip) | - |
| Rector          | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://media.kaspersky.com/utilities/VirusUtilities/EN/rectordecryptor.zip) | - |
| Scatter         | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://media.kaspersky.com/utilities/VirusUtilities/EN/ScatterDecryptor.zip) | - |
| SZFLocker       | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://files-download.avg.com/util/avgrem/avg_decryptor_SzfLocker.exe) | - |
| Jigsaw          | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://download.bleepingcomputer.com/demonslay335/JigSawDecrypter.zip) | - |
| TeslaCrypt      | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://www.bleepingcomputer.com/news/security/teslacrypt-shuts-down-and-releases-master-decryption-key/) | - |
| TeslaCrypt      | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://esupport.trendmicro.com/solution/en-US/1114221.aspx) | - |
| TeslaCrypt      | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://files-download.avg.com/util/avgrem/avg_decryptor_TeslaCrypt3.exe) | - |
| Locker          | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](https://drive.google.com/file/d/0B6y1wjhVZ-WPVkttLVdIWXNodkk/view?usp=sharing) | - |
| Torlocker       | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://media.kaspersky.com/utilities/VirusUtilities/EN/ScraperDecryptor.zip) | - |
| Linux.Encoder.1 | [![goto-download](https://img.shields.io/badge/Goto-Download-green.svg)](http://labs.bitdefender.com/wp-content/plugins/download-monitor/download.php?id=Decrypter_0-1.3.zip) | - |
| Linux.Encoder.1  | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](https://github.com/eugenekolo/linux-ransomware-decrypter) | - |
| CryptoLocker*   | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](https://www.fireeye.com/blog/executive-perspective/2014/08/your-locker-of-information-for-cryptolocker-decryption.html) | Discontinued |
| CryptoTorLocker2015 | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](http://www.bleepingcomputer.com/news/security/help-recover-files-txt-ransomware-installed-by-targeted-terminal-services-attacks/) | - |
| Petya           | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](https://github.com/hasherezade/petya_recovery) | - |
| SNSLocker       | [![goto-url](https://img.shields.io/badge/Goto-URL-brightgreen.svg)](https://esupport.trendmicro.com/solution/en-US/1114221.aspx) | - |
| zCrypt          | NA | NA |

<p><strong><a href="http://blog.emsisoft.com/2016/12/30/emsisoft-releases-free-decrypter-for-opentoyou-ransomware/" target="_blank" rel="noopener">OpenToYou decryption tools</a></strong></p>

<p><strong><a href="http://blog.emsisoft.com/2017/01/04/emsisoft-releases-free-decrypter-for-globe3-ransomware/" target="_blank" rel="noopener">Globe3 decryption tool </a></strong></p>

<p><strong><a href="https://www.nomoreransom.org/decryption-tools.html" target="_blank" rel="noopener">Dharma Decryptor</a></strong></p>

<p><strong><a href="https://www.bleepingcomputer.com/news/security/emsisoft-releases-a-decryptor-for-the-crypton-ransomware/" target="_blank" rel="noopener">CryptON decryption tool</a></strong></p>

<p><strong><a href="https://www.avast.com/ransomware-decryption-tools" target="_blank" rel="noopener">Alcatraz Decryptor tool</a></strong> // <strong><a href="http://files.avast.com/files/decryptor/avast_decryptor_alcatrazlocker.exe" target="_blank" rel="noopener">direct tool download</a></strong></p>

<p><strong><a href="http://files.avast.com/files/decryptor/avast_decryptor_hiddentear.exe" target="_blank" rel="noopener">HiddenTear decryptor (Avast)</a></strong></p>

<p><strong><a href="http://files.avast.com/files/decryptor/avast_decryptor_noobcrypt.exe" target="_blank" rel="noopener">NoobCrypt decryptor (Avast)</a></strong></p>

<p><strong><a href="http://files.avast.com/files/decryptor/avast_decryptor_cryptomix.exe" target="_blank" rel="noopener">CryptoMix/CryptoShield decryptor tool for offline key (Avast)</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/damage" target="_blank" rel="noopener">Damage ransomware decryption tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/777" target="_blank" rel="noopener">.777 ransomware decrypting tool</a></strong></p>

<p><strong><a href="https://github.com/hasherezade/malware_analysis/tree/master/7ev3n" target="_blank" rel="noopener">7even-HONE$T decrypting tool</a></strong></p>

<p><strong><a href="https://download.bleepingcomputer.com/demonslay335/hidden-tear-bruteforcer.zip" target="_blank" rel="noopener">.8lock8 ransomware decrypting tool</a></strong> + <strong><a href="http://www.bleepingcomputer.com/forums/t/614025/8lock8-help-support-topic-8lock8-read-ittxt/" target="_blank" rel="noopener">explanations</a></strong></p>

<p><strong><a href="https://github.com/hasherezade/malware_analysis/tree/master/7ev3n" target="_blank" rel="noopener">7ev3n decrypting tool</a></strong></p>

<p><strong><u><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip" target="_blank" rel="noopener">AES_NI Rakhni Decryptor tool</a></u></strong></p>

<p><strong><a href="https://www.nomoreransom.org/decryption-tools.html" target="_blank" rel="noopener">Agent.iih</a> decrypting tool (decrypted by the Rakhni Decryptor)</strong></p>

<p><strong><u><a href="https://files.avast.com/files/decryptor/avast_decryptor_alcatrazlocker.exe" target="_blank" rel="noopener">Alcatraz Ransom decryptor tool</a></u></strong></p>

<p><strong><a href="https://cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=d4173312-989b-4721-ad00-8308fff353b3&amp;placement_guid=22f2fe97-c748-4d6a-9e1e-ba3fb1060abe&amp;portal_id=326665&amp;redirect_url=APefjpGnqFjmP_xzeUZ1Y55ovglY1y1ch7CgMDLit5GTHcW9N0ztpnIE-ZReqqv8MDj687_4Joou7Cd2rSx8-De8uhFQAD_Len9QpT7Xvu8neW5drkdtTPV7hAaou0osAi2O61dizFXibewmpO60UUCd5OazCGz1V6yT_3UFMgL0x9S1VeOvoL_ucuER8g2H3f1EfbtYBw5QFWeUmrjk-9dGzOGspyn303k9XagBtF3SSX4YWSyuEs03Vq7Fxb04KkyKc4GJx-igK98Qta8iMafUam8ikg8XKPkob0FK6Pe-wRZ0QVWIIkM&amp;hsutk=34612af1cd87864cf7162095872571d1&amp;utm_referrer=https%3A%2F%2Finfo.phishlabs.com%2Fblog%2Falma-ransomware-analysis-of-a-new-ransomware-threat-and-a-decrypter&amp;canon=https%3A%2F%2Finfo.phishlabs.com%2Fblog%2Falma-ransomware-analysis-of-a-new-ransomware-threat-and-a-decrypter&amp;__hstc=61627571.34612af1cd87864cf7162095872571d1.1472135921345.1472140656779.1472593507113.3&amp;__hssc=61627571.1.1472593507113&amp;__hsfp=1114323283" target="_blank" rel="noopener">Alma decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/al-namrood" target="_blank" rel="noopener">Al-Namrood decrypting tool</a></strong><u> </u></p>

<p><strong><a href="https://www.bleepingcomputer.com/download/alphadecrypter/" target="_blank" rel="noopener">Alpha decrypting tool</a></strong></p>

<p><strong><a href="https://www.bleepingcomputer.com/download/alphadecrypter/" target="_blank" rel="noopener">AlphaLocker decrypting tool</a></strong></p>

<p><strong><u><a href="https://decrypter.emsisoft.com/download/amnesia" target="_blank" rel="noopener">Amnesia Ransom decryptor tool</a></u></strong></p>

<p><strong><u><a href="https://decrypter.emsisoft.com/download/amnesia2" target="_blank" rel="noopener">Amnesia Ransom 2 decryptor tool</a></u></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/apocalypse" target="_blank" rel="noopener">Apocalypse decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/apocalypsevm" target="_blank" rel="noopener">ApocalypseVM decrypting tool</a></strong> + <strong><a href="http://www.avg.com/us-en/ransomware-decryption-tools#apocalypse" target="_blank" rel="noopener">alternative</a></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip" target="_blank" rel="noopener">Aura decrypting tool</a></strong> (decrypted by the Rakhni Decryptor)</p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rannohdecryptor.zip" target="_blank" rel="noopener">AutoIt decrypting tool</a></strong> (decrypted by the Rannoh Decryptor)</p>

<p><strong><a href="https://decrypter.emsisoft.com/autolocky" target="_blank" rel="noopener">Autolocky decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/badblock" target="_blank" rel="noopener">Badblock decrypting tool</a></strong> + <strong><a href="http://www.avg.com/us-en/ransomware-decryption-tools#badblock%20+" target="_blank" rel="noopener">alternative 1</a></strong></p>

<p><strong><u><a href="http://blog.checkpoint.com/wp-content/uploads/2017/03/BarRaxDecryptor.zip" target="_blank" rel="noopener">BarRax Ransom decryption tool</a></u></strong></p>

<p><strong><a href="http://www.avg.com/us-en/ransomware-decryption-tools#bart" target="_blank" rel="noopener">Bart decrypting tool</a></strong></p>

<p><strong><a href="https://www.nomoreransom.org/uploads/CoinVaultDecryptor.zip" target="_blank" rel="noopener">BitCryptor decrypting tool</a></strong></p>

<p><strong><a href="https://download.bleepingcomputer.com/demonslay335/BitStakDecrypter.zip" target="_blank" rel="noopener">BitStak decrypting tool</a></strong></p>

<p><strong><u><a href="https://files.avast.com/files/decryptor/avast_decryptor_btcware.exe" target="_blank" rel="noopener">BTCWare Ransom decryptor</a></u></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip" target="_blank" rel="noopener">Chimera decrypting tool</a></strong> + <strong><a href="https://www.nomoreransom.org/decryption-tools.html" target="_blank" rel="noopener">alternative 1</a></strong> + <strong><a href="https://success.trendmicro.com/portal_kb_articledetail?solutionid=1114221" target="_blank" rel="noopener">alternative 2</a></strong></p>

<p><strong><a href="https://www.nomoreransom.org/decryption-tools.html" target="_blank" rel="noopener">CoinVault decrypting tool</a></strong></p>

<p><u><a href="https://decrypter.emsisoft.com/download/cry128" target="_blank" rel="noopener"><strong>Cry128 decrypting tool</strong></a></u></p>

<p><strong><u><a href="https://decrypter.emsisoft.com/download/cry9" target="_blank" rel="noopener">Cry9 Ransom decrypting tool</a></u></strong></p>

<p><strong><a href="https://www.nomoreransom.org/decryption-tools.html" target="_blank" rel="noopener">Cryakl decrypting tool</a></strong> (decrypted by the Rannoh Decryptor)</p>

<p><strong><a href="https://www.nomoreransom.org/decryption-tools.html" target="_blank" rel="noopener">Crybola decrypting tool</a></strong> (decrypted by the Rannoh Decryptor)</p>

<p><strong><a href="https://decrypter.emsisoft.com/crypboss" target="_blank" rel="noopener">CrypBoss decrypting tool</a></strong></p>

<p><strong><a href="https://github.com/pekeinfo/DecryptCrypren" target="_blank" rel="noopener">Crypren decrypting tool</a></strong></p>

<p><strong><a href="https://blog.fortinet.com/2016/06/17/buggy-russian-ransomware-inadvertently-allows-free-decryption" target="_blank" rel="noopener">Crypt38 decrypting tool</a></strong></p>

<p><strong><a href="http://www.avg.com/us-en/ransomware-decryption-tools#crypt888" target="_blank" rel="noopener">Crypt888 (see also Mircop) decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/cryptinfinite" target="_blank" rel="noopener">CryptInfinite decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/cryptodefense" target="_blank" rel="noopener">CryptoDefense decrypting tool</a></strong></p>

<p><strong><a href="http://www.bleepingcomputer.com/news/security/cryptohost-decrypted-locks-files-in-a-password-protected-rar-file/" target="_blank" rel="noopener">CryptoHost (a.k.a. Manamecrypt) decrypting tool</a></strong></p>

<p><strong><a href="https://www.nomoreransom.org/decryption-tools.html" target="_blank" rel="noopener">Cryptokluchen decrypting tool</a></strong> (decrypted by the Rakhni Decryptor)</p>

<p><strong><u><a href="https://nomoreransom.cert.pl/static/cryptomix_decryptor.exe" target="_blank" rel="noopener">CryptoMix Ransom decrypting tool</a></u></strong></p>

<p><strong><a href="http://www.bleepingcomputer.com/forums/t/565020/new-cryptotorlocker2015-ransomware-discovered-and-easily-decrypted/" target="_blank" rel="noopener">CryptoTorLocker decrypting tool</a></strong></p>

<p><strong><a href="https://blog.kaspersky.com/cryptxxx-decryption-20/12091/" target="_blank" rel="noopener">CryptXXX decrypting tool</a></strong></p>

<p><strong><a href="https://www.nomoreransom.org/decryption-tools.html" target="_blank" rel="noopener">CrySIS decrypting tool</a></strong><u> (decrypted by the Rakhni Decryptor – </u><strong><a href="https://www.bleepingcomputer.com/news/security/master-decryption-keys-and-decryptor-for-the-crysis-ransomware-released-/" target="_blank" rel="noopener">additional details</a></strong><u>)</u></p>

<p><strong><a href="https://thisissecurity.net/2016/02/26/a-lockpicking-exercise/" target="_blank" rel="noopener">CTB-Locker Web decrypting tool</a></strong></p>

<p><strong><a href="https://github.com/aaaddress1/my-Little-Ransomware/tree/master/decryptoTool" target="_blank" rel="noopener">CuteRansomware decrypting tool</a></strong></p>

<p><strong><u><a href="https://decrypter.emsisoft.com/download/damage" target="_blank" rel="noopener">Damage ransom decrypting tool</a></u></strong></p>

<p><strong><u><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip" target="_blank" rel="noopener">Dharma Ransom Rakhni decryptor tool</a></u></strong></p>

<p><strong><a href="http://tmp.emsisoft.com/fw/decrypt_mblblock.exe" target="_blank" rel="noopener">DeCrypt Protect decrypting tool</a></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip" target="_blank" rel="noopener">Democry decrypting tool</a></strong> (decrypted by the Rakhni Decryptor)</p>

<p><strong><u><a href="http://blog.checkpoint.com/wp-content/uploads/2016/12/Derialock-Decryptor.zip" target="_blank" rel="noopener">Derialock ransom decryptor tool</a></u></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/dmalocker" target="_blank" rel="noopener">DMA Locker decrypting tool</a></strong> + <strong><a href="https://decrypter.emsisoft.com/dmalocker2" target="_blank" rel="noopener">DMA2 Locker decoding tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/fabiansomware" target="_blank" rel="noopener">Fabiansomware decrypting tool</a></strong></p>

<p><a href="https://www.bleepingcomputer.com/news/security/decryptor-released-for-the-everbe-ransomware/" target="_blank" rel="noopener"><strong>Everbe Ransomware decrypting tool</strong> </a></p>

<p><strong><a href="https://blog.avast.com/avast-releases-free-decryption-tool-for-encryptile-ransomware" target="_blank" rel="noopener">Encryptile decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/fenixlocker" target="_blank" rel="noopener">FenixLocker – decrypting tool</a></strong></p>

<p><strong><a href="https://www.nomoreransom.org/decryption-tools.html" target="_blank" rel="noopener">Fury decrypting tool</a></strong> (decrypted by the Rannoh Decryptor)</p>

<p><strong><a href="https://download.bleepingcomputer.com/demonslay335/GhostCryptDecrypter.zip" target="_blank" rel="noopener">GhostCrypt decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/" target="_blank" rel="noopener">Globe / Purge decrypting tool</a></strong> + <strong><a href="https://success.trendmicro.com/portal_kb_articledetail?solutionid=1114221" target="_blank" rel="noopener">alternative</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/gomasom" target="_blank" rel="noopener">Gomasom decrypting tool</a></strong></p>

<p><strong><a href="https://labs.bitdefender.com/2018/02/gandcrab-ransomware-decryption-tool-available-for-free/" target="_blank" rel="noopener">GandCrab decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/harasom" target="_blank" rel="noopener">Harasom decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/hydracrypt" target="_blank" rel="noopener">HydraCrypt decrypting tool</a></strong></p>

<p><strong><u><a href="https://files.avast.com/files/decryptor/avast_decryptor_hiddentear.exe" target="_blank" rel="noopener">HiddenTear decrypting tool</a></u></strong></p>

<p><strong><a href="http://www.bankinfosecurity.com/two-new-ransomware-decryptors-give-victims-free-out-a-9998" target="_blank" rel="noopener">Jaff decrypter tool</a></strong></p>

<p><strong><a href="https://blog.checkpoint.com/wp-content/uploads/2016/07/JPS_release.zip" target="_blank" rel="noopener">Jigsaw/CryptoHit decrypting tool</a></strong> + <strong><a href="http://www.bleepingcomputer.com/download/jigsaw-decrypter/" target="_blank" rel="noopener">alternative</a></strong></p>

<p><strong><a href="http://news.drweb.com/show/?i=9877&amp;lng=en&amp;c=5" target="_blank" rel="noopener">KeRanger decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/keybtc" target="_blank" rel="noopener">KeyBTC decrypting tool</a></strong></p>

<p><strong><a href="https://blog.fortinet.com/2016/04/01/kimcilware-ransomware-how-to-decrypt-encrypted-files-and-who-is-behind-it" target="_blank" rel="noopener">KimcilWare decrypting tool</a></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip" target="_blank" rel="noopener">Lamer decrypting tool</a></strong> (decrypted by the Rakhni Decryptor)</p>

<p><strong><u><a href="https://files.avast.com/files/decryptor/avast_decryptor_lambdalocker.exe" target="_blank" rel="noopener">LambdaLocker decryption tool</a></u></strong></p>

<p><strong><a href="https://success.trendmicro.com/portal_kb_articledetail?solutionid=1114221" target="_blank" rel="noopener">LeChiffre decrypting tool</a></strong> + <strong><a href="https://decrypter.emsisoft.com/lechiffre" target="_blank" rel="noopener">alternative</a></strong></p>

<p><strong><a href="http://www.avg.com/us-en/ransomware-decryption-tools#legion" target="_blank" rel="noopener">Legion decrypting tool</a></strong></p>

<p><strong><a href="https://labs.bitdefender.com/2015/11/linux-ransomware-debut-fails-on-predictable-encryption-key/" target="_blank" rel="noopener">Linux.Encoder decrypting tool</a></strong></p>

<p><strong><a href="https://esupport.trendmicro.com/en-us/home/pages/technical-support/1105975.aspx" target="_blank" rel="noopener">Lock Screen ransomware decrypting tool</a></strong></p>

<p><strong><a href="http://www.bleepingcomputer.com/forums/t/577246/locker-ransomware-support-and-help-topic/page-32#entry3721545" target="_blank" rel="noopener">Locker decrypting tool</a></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip" target="_blank" rel="noopener">Lortok decrypting tool</a></strong> (decrypted by the Rakhni Decryptor)</p>

<p><strong><u><a href="https://decrypter.emsisoft.com/download/marlboro" target="_blank" rel="noopener">Marlboro ransom decryption tool</a></u></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rannohdecryptor.zip" target="_blank" rel="noopener">MarsJoke decryption tool</a></strong></p>

<p><strong><a href="http://www.bleepingcomputer.com/news/security/cryptohost-decrypted-locks-files-in-a-password-protected-rar-file/" target="_blank" rel="noopener">Manamecrypt decrypting tool (a.k.a. CryptoHost)</a></strong></p>

<p><strong><a href="http://now.avg.com/dont-pay-the-ransom-avg-releases-six-free-decryption-tools-to-retrieve-your-files/" target="_blank" rel="noopener">Mircop decrypting tool</a></strong> + <strong><a href="https://success.trendmicro.com/portal_kb_articledetail?solutionid=1114221" target="_blank" rel="noopener">alternative</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/mrcr" target="_blank" rel="noopener">Merry Christmas / MRCR decryptor</a></strong></p>

<p><strong><u><a href="https://nomoreransom.cert.pl/static/mole_decryptor.exe" target="_blank" rel="noopener">Mole decryptor tool</a></u></strong></p>

<p><strong><a href="https://github.com/Cyberclues/nanolocker-decryptor" target="_blank" rel="noopener">Nanolocker decrypting tool</a></strong></p>

<p><strong><a href="https://success.trendmicro.com/portal_kb_articledetail?solutionid=1114221" target="_blank" rel="noopener">Nemucod decrypting tool</a></strong> + <strong><a href="https://decrypter.emsisoft.com/nemucod" target="_blank" rel="noopener">alternative</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/download/nmoreira" target="_blank" rel="noopener">NMoreira ransomware decryption tool</a></strong></p>

<p><strong><a href="https://files.avast.com/files/decryptor/avast_decryptor_noobcrypt.exe" target="_blank" rel="noopener">Noobcrypt decryption tool</a></strong></p>

<p><strong><a href="http://download.bleepingcomputer.com/BloodDolly/ODCODCDecoder.zip" target="_blank" rel="noopener">ODCODC decrypting tool</a></strong></p>

<p><strong><a href="http://www.bleepingcomputer.com/forums/t/559220/operation-global-iii-ransomware-not-only-encrypts-but-infects-your-data-as-well/" target="_blank" rel="noopener">Operation Global III Ransomware decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/download/ozozalocker" target="_blank" rel="noopener">Ozozalocker ranomware decryptor</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/pclock" target="_blank" rel="noopener">PClock decrypting tool</a></strong></p>

<p><strong><a href="http://www.thewindowsclub.com/petya-ransomware-decrypt-tool-password-generator" target="_blank" rel="noopener">Petya decrypting tool</a></strong> + <strong><a href="https://www.bleepingcomputer.com/news/security/author-of-original-petya-ransomware-publishes-master-decryption-key/" target="_blank" rel="noopener">alternative</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/download/philadelphia" target="_blank" rel="noopener">Philadelphia decrypting tool</a></strong></p>

<p><strong><a href="http://download.bleepingcomputer.com/BloodDolly/JuicyLemonDecoder.zip" target="_blank" rel="noopener">PizzaCrypts decrypting tool</a></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip" target="_blank" rel="noopener">Pletor decrypting tool</a></strong> (decrypted by the Rakhni Decryptor)</p>

<p><strong><a href="http://www.bleepingcomputer.com/news/security/pompous-ransomware-dev-gets-defeated-by-backdoor/" target="_blank" rel="noopener">Pompous decrypting tool</a></strong></p>

<p><strong><a href="https://www.helpnetsecurity.com/2016/07/22/powerware-ransomware-decrypter/" target="_blank" rel="noopener">PowerWare / PoshCoder decrypting tool</a></strong></p>

<p><strong><a href="https://www.elevenpaths.com/downloads/RecoverPopCorn.zip" target="_blank" rel="noopener">Popcorn Ransom decrypting tool</a></strong></p>

<p><strong><a href="https://decrypter.emsisoft.com/radamant" target="_blank" rel="noopener">Radamant decrypting tool</a></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip" target="_blank" rel="noopener">Rakhni decrypting tool</a></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rannohdecryptor.zip" target="_blank" rel="noopener">Rannoh decrypting tool</a></strong></p>

<p><strong><a href="https://support.kaspersky.com/viruses/disinfection/4264" target="_blank" rel="noopener">Rector decrypting tool</a></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip" target="_blank" rel="noopener">Rotor decrypting tool</a></strong> (decrypted by the Rakhni Decryptor)</p>

<p><strong><a href="https://securelist.com/a-flawed-ransomware-encryptor/69481/" target="_blank" rel="noopener">Scraper decrypting tool</a></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/ShadeDecryptor.exe" target="_blank" rel="noopener">Shade / Troldesh decrypting tool + </a></strong><strong><a href="http://www.mcafee.com/us/downloads/free-tools/shadedecrypt.aspx" target="_blank" rel="noopener">alternative</a></strong></p>

<p><strong><a href="https://success.trendmicro.com/portal_kb_articledetail?solutionid=1114221" target="_blank" rel="noopener">SNSLocker decrypting tool</a></strong></p>

<p><strong><a href="https://success.trendmicro.com/portal_kb_articledetail?solutionid=1114221" target="_blank" rel="noopener">Stampado decrypting tool</a></strong> + <strong><a href="https://decrypter.emsisoft.com/stampado" target="_blank" rel="noopener">alternative</a></strong></p>

<p><strong><a href="http://www.avg.com/us-en/ransomware-decryption-tools#szflocker" target="_blank" rel="noopener">SZFlocker decrypting tool</a></strong></p>

<p><strong><a href="https://success.trendmicro.com/solution/1114221" target="_blank" rel="noopener">Teamxrat / Xpan decryption tool</a></strong></p>

<p><strong><a href="https://malwarebytes.app.box.com/s/kkxwgzbpwe7oh59xqfwcz97uk0q05kp3" target="_blank" rel="noopener">TeleCrypt decrypting tool</a></strong><u> (</u><strong><a href="https://blog.malwarebytes.com/threat-analysis/2016/11/telecrypt-the-ransomware-abusing-telegram-api-defeated/" target="_blank" rel="noopener">additional details</a></strong><u>)</u></p>

<p><strong><a href="http://talosintel.com/teslacrypt_tool/" target="_blank" rel="noopener">TeslaCrypt decrypting tool</a></strong> + <strong><a href="http://www.avg.com/us-en/ransomware-decryption-tools#teslacrypt" target="_blank" rel="noopener">alternative 1</a></strong> + <strong><a href="https://www.nomoreransom.org/decryption-tools.html" target="_blank" rel="noopener">alternative 2</a></strong></p>

<p><strong><a href="http://www.bleepingcomputer.com/forums/t/547708/torrentlocker-ransomware-cracked-and-decrypter-has-been-made/" target="_blank" rel="noopener">TorrentLocker decrypting tool</a></strong></p>

<p><strong><a href="http://www.thewindowsclub.com/emsisoft-decrypter-hydracrypt-umbrecrypt-ransomware" target="_blank" rel="noopener">Umbrecrypt decrypting tool</a></strong></p>

<p><strong><a href="http://media.kaspersky.com/utilities/VirusUtilities/RU/WildfireDecryptor.zip" target="_blank" rel="noopener">Wildfire decrypting tool + </a></strong><strong><a href="http://www.mcafee.com/us/downloads/free-tools/wildfiredecrypt.aspx" target="_blank" rel="noopener">alternative</a></strong></p>

<p><strong><a href="https://github.com/gentilkiwi/wanakiwi/releases" target="_blank" rel="noopener">WannaCry decryption tool</a></strong> + <strong><a href="https://blog.comae.io/wannacry-decrypting-files-with-wanakiwi-demo-86bafb81112d" target="_blank" rel="noopener">Guide</a></strong></p>

<p><strong><u><a href="http://media.kaspersky.com/utilities/VirusUtilities/EN/rakhnidecryptor.zip" target="_blank" rel="noopener">XData Ransom decryption tool</a></u></strong></p>

<p><strong><a href="https://success.trendmicro.com/portal_kb_articledetail?solutionid=1114221" target="_blank" rel="noopener">XORBAT decrypting tool</a></strong></p>

<p><strong><a href="https://success.trendmicro.com/portal_kb_articledetail?solutionid=1114221" target="_blank" rel="noopener">XORIST decrypting tool</a></strong> + <strong><a href="https://decrypter.emsisoft.com/xorist" target="_blank" rel="noopener">alternative</a></strong><br />

<strong><a href="https://nioguard.blogspot.com.br/2018/02/decryptor-for-moneropay-ransomware.html" target="_blank" rel="noopener">MoneroPay Ransomware decrypting tool</a></strong></p>
